ARG ARDUINO_VERSION=1.8.15

# First stage build to download and extract Arduino IDE installer
FROM arm32v7/debian:latest AS download

# Update and install necessary packages
RUN apt-get update && apt-get dist-upgrade --yes
RUN apt-get install -y wget unzip pkg-config make g++ libftdi1-2 libftdi1-dev libhidapi-libusb0 libhidapi-dev libudev-dev cmake

# Download and extract the archive
RUN wget https://github.com/trabucayre/openFPGALoader/archive/refs/heads/master.zip
RUN unzip master.zip
RUN rm master.zip

# Make and install openFPGALoader
WORKDIR /openFPGALoader-master
RUN mkdir build
WORKDIR /openFPGALoader-master/build
RUN cmake ../
RUN cmake --build .
RUN make install